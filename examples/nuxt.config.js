export default {
  // Global page headers (https://go.nuxtjs.dev/config-head)
  head: {
    title: 'httpheader-example',
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { hid: 'description', name: 'description', content: '' }
    ],
    link: [
      { rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' }
    ]
  },

  // Auto import components (https://go.nuxtjs.dev/config-components)
  components: true,

  // Modules (https://go.nuxtjs.dev/config-modules)
  modules: [
  // Module with fonctions options and parameters
    ['@qonfucius/nuxt-headers', {
      serverHeaders: (req) => ({
        'Host': `${req.headers.host}`,
      }),
      nuxtHeaders: (context) => ({
        'Cache-Control': `max-age=${context.app.router.currentRoute.meta.cache}, public`,
      }),
    }],

  //module with serverHeader and nuxtHeader objects options without parameters
   /*['@qonfucius/nuxt-headers', {
        serverHeaders: {
          'Accept-Ranges': 'bytes',
        },
        nuxtHeaders: {
          'Other-header': 'new',
        },
    }],*/ 
    
  ],
  //httpHeaders.serverHeaders and nuxtHeaders
  httpHeaders: {
    serverHeaders: {
      'New-header': 'what you want',
    },
    nuxtHeaders: {
      'Other-header': 'new',
    },
  },
}
