# Middleware (nuxtHeaders)

## Add nuxtHeaders
To add nuxtHeaders, you have to implement a serverHeaders `Object` or `function` in `nuxt.config.js`. It must return a list of headers.

### Object
You can pass nuxtHeaders directly in options in the module.
https://fr.nuxtjs.org/docs/2.x/directory-structure/modules/

Example:
```js
//nuxt.config.js

 modules: [
  // Module with nuxtHeaders objects options 
   ['@qonfucius/nuxt-headers', {
        nuxtHeaders: {
          'Accept-Ranges': 'bytes',
          'Other-header': 'new',
        },
    }],
  ],
```

Also, you can pass nuxtHeaders in config key.

Example:
```js
  // nuxt.config.js
  modules: [
    '@qonfucius/nuxt-headers', 
  ],

  httpHeaders: {
 //httpHeaders.nuxtHeaders 
    nuxtHeaders: {
      'New-header': 'what you want',
      'Accept-Ranges': 'bytes',
    },
  },
```

### Function
Via nuxtHeaders, it will add to every page access, via middleware. You can access to `context` in parameters.
Like object, you can pass nuxtHeaders function in module or config key.

Example:
```js
//nuxt.config.js
  modules: [
  // Module with fonctions options and parameters
    ['@qonfucius/nuxt-headers', {
      nuxtHeaders: (context) => ({
        'Cache-Control': `max-age=${context.app.router.currentRoute.meta.cache}, public`,
      }),
    }],
  ]
```
or

Example:
```js
//nuxt.config.js
 modules: [
    '@qonfucius/nuxt-headers', 
  ],

  httpHeaders: {
 //httpHeaders.nuxtHeaders 
    nuxtHeaders: (context) => ({
        'Cache-Control': `max-age=${context.app.router.currentRoute.meta.cache}, public`,
      }),
  },
```

## More
It is possible to add serverHeaders in the module or in configuration keys.

Example:
```js
//nuxt.config.js
 modules: [
  // Module with fonctions options and parameters
    ['@qonfucius/nuxt-headers', {
      serverHeaders: (req) => ({
        'Host': `${req.headers.host}`,
      }),
      nuxtHeaders: () => ({
        'Cache-Control': `max-age=99, public`,
      }),
    }],
 ]
```

**Pay attention to priorities.
https://gitlab.com/qonfucius/nuxt-headers#priority**

## Open example project
Inside `examples` folder
```bash
# serve with hot reload at localhost:3000
$ npm run dev
```
