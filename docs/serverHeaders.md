# Server Middleware (serverHeaders)

## Add serverHeaders
To add serverHeaders, you need to implement a serverHeaders`Object` or `function` in `nuxt.config.js`. It must return a list of headers.

### Object
You can pass serverHeaders directly in options in the module.
https://fr.nuxtjs.org/docs/2.x/directory-structure/modules/

Example:
```js
//nuxt.config.js
 modules: [
  // Module with serverHeaders objects options 
   ['@qonfucius/nuxt-headers', {
        serverHeaders: {
          'Accept-Ranges': 'bytes',
          'Other-header': 'new',
        },
    }],
  ],
```

Also, you can pass serverHeaders in config key.

Example:
```js
  // nuxt.config.js
  modules: [
    '@qonfucius/nuxt-headers', 
  ],

  httpHeaders: {
 //httpHeaders.serverHeaders 
    serverHeaders: {
      'New-header': 'what you want',
      'Accept-Ranges': 'bytes',
    },
  },
```

### Function
Via serverHeaders, it will add to every request, via server middleware, including serving assets, favicon...You can access to `req` in parameters.
Like object, you can pass serverHeaders function in module or config key.

Example:
```js
//nuxt.config.js
  modules: [
  // Module with fonctions options and parameters
    ['@qonfucius/nuxt-headers', {
      serverHeaders: (req) => ({
        'Host': `${req.headers.host}`,
      }),
    }],
  ]
```
or

Example:
```js
//nuxt.config.js
 modules: [
    '@qonfucius/nuxt-headers', 
  ],

  httpHeaders: {
 //httpHeaders.serverHeaders 
    serverHeaders: (req) => ({
        'Host': `${req.headers.host}`,
      }),
  },
```

## More
It is possible to add nuxtHeaders in the module or in configuration keys.

Example:
```js
//nuxt.config.js
 modules: [
  // Module with fonctions options and parameters
    ['@qonfucius/nuxt-headers', {
      serverHeaders: (req) => ({
        'Host': `${req.headers.host}`,
      }),
      nuxtHeaders: () => ({
        'Cache-Control': `max-age=99, public`,
      }),
    }],
 ]
```

**Pay attention to priorities.
https://gitlab.com/qonfucius/nuxt-headers#priority**

## Open example project
Inside `examples` folder
```bash
# serve with hot reload at localhost:3000
$ npm run dev
```
