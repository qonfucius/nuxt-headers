# Custom Nuxt Headers
[Documentation](https://qonfucius.gitlab.io/nuxt-headers/)

## Installation

Add this library in dependencies, then add it to nuxtJS modules
```sh
npm install -s @qonfucius/nuxt-headers
```

```js
// nuxt.config.js
export default {
  modules: [
    '@qonfucius/nuxt-headers',
  ],
};
```

## Usage
With this module you will be able to add custom headers in many ways : 
- Via `serverHeaders`, it will add to every request, via server middleware, including serving assets, favicon...You can access to `req` in parameters.
[Documentation](https://qonfucius.gitlab.io/nuxt-headers/serverHeaders.html)

- Via `nuxtHeaders`, it will add to every page access, via middleware.
You can access to `context` in parameters.
[Documentation](https://qonfucius.gitlab.io/nuxt-headers/nuxtHeaders.html)

- In vue scripts, you can add a key `httpHeaders`, with same behavior than `nuxtHeaders`, but specific for a page.
[Documentation](https://qonfucius.gitlab.io/nuxt-headers/httpHeaders.html)


### Priority

_Lowest in list is prior_

- All headers in config key `httpHeaders.serverHeaders` are set
- All headers in config key `httpHeaders.nuxtHeaders` are set
- All headers in module related configuration `serverHeaders` are set
- All headers in module related configuration `nuxtHeaders` are set
- All headers in vue file are set.

## Special thanks

- To [MartinLG for the module "nuxt-custom-headers"](https://github.com/MartinLG/nuxt-custom-headers), especially for the method to get .vue information.
