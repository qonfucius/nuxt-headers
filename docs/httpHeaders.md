# Inside Pages (httpHeaders)

## Add headers to your pages
To add headers to a page, you have to implement a httpHeaders `Object` in the page component. It must return a list of headers.

Example: 

```html
//example.vue
<template>
  <div>
    <p>This is an awesome page</p>
  </div>
</template>
```
```js
<script>
  export default {
      name: 'MyPage',        
      httpHeaders: {
        'Cache-Control': 'max-age=60, s-maxage=90, public',
        'X-My-Header': 'Anything you could need'
      }
  }
</script>
```

Or with a `function`

Example: 

```html
//example.vue
<template>
  <div>
    <p>This is an awesome page</p>
  </div>
</template>
```
```js
<script>
  export default {
      name: 'MyPage',        
      httpHeaders: () => ({
        'Cache-Control': 'max-age=60, s-maxage=90, public',
        'X-My-Header': 'Anything you could need'
      })
  }
</script>
```


## Use Nuxt context

You can use the Nuxt context in the httpHeaders to generate headers based on the router or any other context related data.

```js
//page.vue
<script>
  export default {
    name: 'example',
    httpHeaders:(context) => ({ 
    'X-Content-Type-Options': 'nosniff',
    'X-My-Header': 'Anything you could need',
    'Cache-Control': `max-age=${context.app.router.currentRoute.meta.cache}, public`
    }),
  }
</script>
```

**Please note, the object has priority. You must choose the object or function.**


## Open example project
Inside `examples` folder
```bash
# serve with hot reload at localhost:3000
$ npm run dev
```
