import { resolve, join } from 'path';

const MODULE_DIR = 'qnuxtheaders';

export default async function nuxtCustomHeaders (options = {}) {
    const OPTIONS_KEY = options.OPTIONS_KEY || 'httpHeaders';

    this.addPlugin({
        src: resolve(__dirname, 'middleware.js'),
        fileName: join(MODULE_DIR, 'middleware.js'),
        options: {
            OPTIONS_KEY,
            nuxtHeaders: [
                (this.options[OPTIONS_KEY] || {}).nuxtHeaders,
                options.nuxtHeaders,
            ],
        }
    });
    this.options.router.middleware.push(OPTIONS_KEY);

    const serverHeaders = [
        (this.options[OPTIONS_KEY] || {}).serverHeaders,
        options.serverHeaders,
    ];
    this.addServerMiddleware((req, res, next) => {
        for (const source of serverHeaders) {
            if (typeof source === 'object' || typeof source === 'function') {
                const headers = typeof source === 'function' ? source(req) : source;
                for(const key of Object.keys(headers)) {
                    res.setHeader(key, headers[key]);
                }
            }
        }
        next();
    });
};

