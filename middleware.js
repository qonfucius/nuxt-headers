import middleware from '../middleware';

middleware['<%=options.OPTIONS_KEY%>'] = async (context) => {
    if (process.server) {
    <% for (const source of options.nuxtHeaders) { %>
        <% if (typeof source === 'object' || typeof source === 'function') { %>
        {
            <% if (typeof source === 'function') { %>
            const headers = (<%=source.toString()%>)(context);
            <% } else { %>
            const headers = <%=JSON.stringify(source) %>;
            <% } %>
            for(const key of Object.keys(headers)) {
                context.res.setHeader(key, headers[key]);
            }
        }
        <% } %>
    <% } %>
        context.route.matched
            .map(({ components }) => Object.values(components))
            .reduce((acc, row) => acc.concat(row), [])
            .forEach(component => {
            const object = (component.options && component.options["<%=options.OPTIONS_KEY%>"])
            const headers = {};
            if (typeof object === 'function') {
                Object.assign(headers, object(context));
            } else if (typeof object === 'object') {
                Object.assign(headers, object);
            }
            for(const key of Object.keys(headers)) {
                context.res.setHeader(key, headers[key]);
            }
        });
    }
}
