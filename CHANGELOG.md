# Changelog

### [v1.0.1](https://gitlab.com/qonfucius/nuxt-headers/compare/v1.0.0...v1.0.1) (2020-11-07)

#### Fixes

* Fix server middleware 36bd7d6


## v1.0.0 (2020-11-06)

### Features

* First module release 652e634


